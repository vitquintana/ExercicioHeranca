/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herancaedep;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import org.bigolin.Conexao;

/**
 *
 * @author Vitória Quintana <vithh.quintana@gmail.com>
 */
public class FuncAdm extends Funcionario {

    private String setor;
    private String funcao;
    private String disciplina;
    private int cod;

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    @Override
    public void inserirPessoa() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OO_Pessoas (nome,idade,endereco,funcao,disciplina,salario) VALUES (?,?,?,?,?,?)";

        try {
            PreparedStatement prepareStatement = dbConnection.prepareStatement(insertTableSQL);

            prepareStatement.setString(1, getNome());
            prepareStatement.setInt(2, getIdade());
            prepareStatement.setString(3, getEndereco());
            prepareStatement.setString(4, getFuncao());
            prepareStatement.setString(5, getDisciplina());
            prepareStatement.setDouble(6, getSalario());

            prepareStatement.executeUpdate();

            System.out.println("Funcionario!!");

        } catch (SQLException e) {
        }
    }

    public static ArrayList<Pessoa> getAll() {
        String select = "SELECT * FROM OO_Pessoas";
        ArrayList<Pessoa> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                FuncAdm funcionario = new FuncAdm();
                funcionario.setNome(rs.getString("Nome"));
                funcionario.setIdade(rs.getInt("Idade"));
                funcionario.setEndereco(rs.getString("Endereço"));
                funcionario.setFuncao(rs.getString("Função"));
                funcionario.setSalario(rs.getDouble("Salario"));
                funcionario.setSetor(rs.getString("Setor"));
                lista.add(funcionario);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }

    @Override
    public void updatePessoa() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String updateTableSQL = "UPDATE Aluno SET nome=?,idade=?,endereco=?,funcao=?,salario=?,setor=? WHERE cod=?";
        try {
            preparedStatement = dbConnection.prepareStatement(updateTableSQL);

            preparedStatement.setString(1, this.getNome());
            preparedStatement.setInt(2, this.getIdade());
            preparedStatement.setString(3, this.getEndereco());
            preparedStatement.setString(4, this.getFuncao());
            preparedStatement.setDouble(5, this.getSalario());
            preparedStatement.setString(6, this.getSetor());

            preparedStatement.executeUpdate();
            System.out.println("Funcionario!!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getOne() {
        String selectSQL = "SELECT * FROM OO_Pessoas WHERE cod=?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.cod);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                this.setNome(rs.getString("Nome"));
                this.setIdade(rs.getInt("Idade"));
                this.setEndereco(rs.getString("Endereco"));
                this.setFuncao(rs.getString("Função"));
                this.setSalario(rs.getDouble("Salario"));
                this.setSetor(rs.getString("Setor"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
