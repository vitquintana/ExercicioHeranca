/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herancaedep;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import org.bigolin.Conexao;

/**
 *
 * @author Vitória Quintana <vithh.quintana@gmail.com>
 */
public  class Funcionario extends Pessoa {
    private double salario;

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
    
    @Override
    public void inserirPessoa() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OO_Pessoas (nome,idade,endereco,funcao,salario) VALUES (?,?,?,?)";

        try {
            PreparedStatement prepareStatement = dbConnection.prepareStatement(insertTableSQL);

            prepareStatement.setString(1, getNome());
            prepareStatement.setInt(2, getIdade());
            prepareStatement.setString(3, getEndereco());
            prepareStatement.setString(4, getFuncao2());
            prepareStatement.setDouble(5, getSalario());


            prepareStatement.executeUpdate();

            System.out.println("Funcionario!!!");

        } catch (SQLException e) {
        }
    }
    
    
    public static ArrayList<Pessoa> getAll() {
        String select = "SELECT * FROM OO_Pessoas";
        ArrayList<Pessoa> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                Funcionario funci = new Funcionario() {
                };
                funci.setNome(rs.getString("Nome"));
                funci.setIdade(rs.getInt("Idade"));
                funci.setEndereco(rs.getString("Endereço"));
                funci.setFuncao2(rs.getString("Função"));
                lista.add(funci);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    
    @Override
     public void updatePessoa() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String updateTableSQL = "UPDATE Aluno SET nome=?,idade=?,endereco=?,funcao=?,salario=? WHERE cod=?";
        try {
            preparedStatement = dbConnection.prepareStatement(updateTableSQL);

            preparedStatement.setString(1, this.getNome());
            preparedStatement.setInt(2, this.getIdade());
            preparedStatement.setString(3, this.getEndereco());
            preparedStatement.setString(4, this.getFuncao2());
            preparedStatement.setDouble(5, this.getSalario());

            preparedStatement.executeUpdate();
            System.out.println("Funcionario!!!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
     
    @Override
     public void getOne() {
        String selectSQL = "SELECT * FROM OO_Pessoas WHERE cod=?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.cod);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                this.setNome(rs.getString("Nome"));
                this.setIdade(rs.getInt("Idade"));
                this.setEndereco(rs.getString("Endereco"));
                this.setFuncao2(rs.getString("Função"));
                this.setSalario(rs.getDouble("Salário"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
