/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herancaedep;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import org.bigolin.Conexao;

/**
 *
 * @author Vitória Quintana <vithh.quintana@gmail.com>
 */
public class Aluno extends Pessoa {
    private String semestre;
    private String curso;
    private int cod;

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }
    
    public void inserirAluno(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
    
        String  insertTableSQL = "INSERT INTO OO_Pessoas (nome,idade,endereco,funcao,curso,semestre) VALUES (?,?,?,?,?,?)";
        
        try {
            PreparedStatement prepareStatement = dbConnection.prepareStatement(insertTableSQL);
            
            prepareStatement.setString(1,getNome());
            prepareStatement.setInt(2,getIdade());
            prepareStatement.setString(3,getEndereco());
            prepareStatement.setString(4,getFuncao2());
            prepareStatement.setString(2,getCurso());
            prepareStatement.setString(2,getSemestre());

            
            prepareStatement.executeUpdate();
        
            System.out.println("Aluno!!!");
       
        }catch(SQLException e){
        }
    }
        
        public static ArrayList<Pessoa> getAll(){
        String select = "SELECT * FROM OO_Pessoas";
        ArrayList <Pessoa>  lista = new ArrayList <>();
        Conexao c = new Conexao ();
        Connection dbConnection = c.getConexao();
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery (select);
            while (rs.next()){
                Aluno al = new Aluno();
                al.setNome (rs.getString("Nome"));
                al.setIdade (rs.getInt("Idade"));
                al.setEndereco (rs.getString("Endereço"));
                al.setFuncao2(rs.getString("Função"));
                al.setCurso(rs.getString("Curso"));
                al.setSemestre(rs.getString("Semestre"));
                lista.add(al);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return lista;
    }
        public void updateAluno () throws SQLException{
          Conexao c = new Conexao();
          Connection dbConnection = c.getConexao();
          PreparedStatement preparedStatement = null;
          
          String updateTableSQL = "UPDATE Aluno SET nome=?,idade=?,endereco=?,funcao=?,curso=?,semestre=? WHERE cod=?";
          try{
          preparedStatement = dbConnection.prepareStatement(updateTableSQL);
          
          preparedStatement.setString(1,this.getNome());
          preparedStatement.setInt(2,this.getIdade());
          preparedStatement.setString(3,this.getEndereco());
          preparedStatement.setString(4,this.getFuncao2());
          preparedStatement.setString(5,this.getCurso());
          preparedStatement.setString(6,this.getSemestre());
          
          preparedStatement.executeUpdate();
          System.out.println("Aluno!!!");
          }catch (SQLException e){
          e.printStackTrace();
          }
          }
        
    @Override
        public void getOne(){
        String selectSQL = "SELECT * FROM OO_Pessoas WHERE cod=?";
        Conexao c = new Conexao ();
        Connection dbConnection = c.getConexao();
        
        PreparedStatement ps;
        try{
        ps = dbConnection.prepareStatement(selectSQL);
        ps.setInt(1,this.cod);
        ResultSet rs  = ps.executeQuery();
        if(rs.next()){
        this.setNome(rs.getString("Nome"));
        this.setIdade(rs.getInt("Idade"));
        this.setEndereco(rs.getString("Endereco"));
        this.setFuncao2(rs.getString("Função"));
        this.setCurso(rs.getString("Curso"));
        this.setSemestre(rs.getString("Semestre"));
        }
        }catch(SQLException e){
        e.printStackTrace();
        }
        
        }
        
        
        
        
        }
          
            
            
            
            
        
        
        
    
    
    
    

