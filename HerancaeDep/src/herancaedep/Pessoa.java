/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herancaedep;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import org.bigolin.Conexao;

/**
 *
 * @author Vitória Quintana <vithh.quintana@gmail.com>
 */
public abstract class Pessoa {

    private String nome;
    private int idade;
    private String endereco;
    private String funcao2;
    public int cod;

    
    public String getFuncao2() {
        return funcao2;
    }

    public void setFuncao2(String funcao2) {
        this.funcao2 = funcao2;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public void inserirPessoa() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OO_Pessoas (nome,idade,endereco,funcao) VALUES (?,?,?,?)";

        try {
            PreparedStatement prepareStatement = dbConnection.prepareStatement(insertTableSQL);

            prepareStatement.setString(1, getNome());
            prepareStatement.setInt(2, getIdade());
            prepareStatement.setString(3, getEndereco());
            prepareStatement.setString(4, getFuncao2());

            prepareStatement.executeUpdate();

            System.out.println("Professor!!!");

        } catch (SQLException e) {
        }
    }

    public static ArrayList<Pessoa> getAll() {
        String select = "SELECT * FROM OO_Pessoas";
        ArrayList<Pessoa> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                Pessoa p = new Pessoa() {
                };
                p.setNome(rs.getString("Nome"));
                p.setIdade(rs.getInt("Idade"));
                p.setEndereco(rs.getString("Endereço"));
                p.setFuncao2(rs.getString("Função"));
                lista.add(p);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void updatePessoa() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String updateTableSQL = "UPDATE Aluno SET nome=?,idade=?,endereco=?,funcao=? WHERE cod=?";
        try {
            preparedStatement = dbConnection.prepareStatement(updateTableSQL);

            preparedStatement.setString(1, this.getNome());
            preparedStatement.setInt(2, this.getIdade());
            preparedStatement.setString(3, this.getEndereco());
            preparedStatement.setString(4, this.getFuncao2());

            preparedStatement.executeUpdate();
            System.out.println("Pessoa!!!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void getOne() {
        String selectSQL = "SELECT * FROM OO_Pessoas WHERE cod=?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.cod);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                this.setNome(rs.getString("Nome"));
                this.setIdade(rs.getInt("Idade"));
                this.setEndereco(rs.getString("Endereco"));
                this.setFuncao2(rs.getString("Função"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
