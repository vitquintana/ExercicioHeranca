/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herancaedep;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import org.bigolin.Conexao;

public class FXMLDocumentController implements Initializable {

    @FXML
    private TableView<Pessoa> table;

   private ObservableList<Pessoa> lista;

    @FXML
    private TableColumn<Pessoa, String> nomecoluna;

    @FXML
    private TableColumn<Pessoa, Integer> idadecoluna;

    @FXML
    private TableColumn<Pessoa, String> enderecocoluna;

    @FXML
    private TableColumn<Pessoa, String> funcaocoluna;

    @FXML
    private TableColumn<Pessoa, String> semestrecoluna;

    @FXML
    private TableColumn<Pessoa, String> cursocoluna;

    @FXML
    private TableColumn<Pessoa, String> setorcoluna;

    @FXML
    private TableColumn<Pessoa, Double> salariocoluna;

    @FXML
    private TableColumn<Pessoa, String> disciplinacoluna;

    @FXML
    private Button button;

    @FXML
    private Label label;

    @FXML
    private RadioButton aluno;

    @FXML
    private RadioButton funcionario;

    @FXML
    private RadioButton professor;

    @FXML
    private AnchorPane tela;

    @FXML
    private TextField nome;

    @FXML
    private TextField endereco;

    @FXML
    private TextField idade;

    @FXML
    private TextField text1;

    @FXML
    private TextField text2;

    @FXML
    private TextField text3;

    @FXML
    private TextField text4;

    @FXML
    private TextField text5;

    @FXML
    private TextField text6;

    @FXML
    private Label nomeg;

    @FXML
    private Label idadeg;

    @FXML
    private Label enderecog;

    @FXML
    private Label disciplina;

    @FXML
    private Label setor;

    @FXML
    private Label funcaog;

    @FXML
    private Label curso;

    @FXML
    private Label salario;

    @FXML
    private Label semestre;

    @FXML
    private Button cadastrar;

    @FXML
    private Button deletar;

    @FXML
    private Button editar;

    @FXML
    private Button mostrar;

   
       

    @FXML
    void editarButton() {

    }

    @FXML
    void mostrarButton() {

    }

    @FXML
    void radioaluno(MouseEvent event) {
        tela.setVisible(true);
        setor.setVisible(false);
        salario.setVisible(false);
        disciplina.setVisible(false);
        setorcoluna.setVisible(false);
        salariocoluna.setVisible(false);
        disciplinacoluna.setVisible(false);
        professor.setVisible(false);
        funcionario.setVisible(false);

    }

    @FXML
    void radiofuncionario(MouseEvent event) {
        tela.setVisible(true);
        semestre.setVisible(false);
        curso.setVisible(false);
        disciplina.setVisible(false);
        semestrecoluna.setVisible(false);
        cursocoluna.setVisible(false);
        disciplinacoluna.setVisible(false);
        aluno.setVisible(false);
        professor.setVisible(false);
    }

    @FXML
    void radioprofessor(MouseEvent event) {
        tela.setVisible(true);
        setor.setVisible(false);
        curso.setVisible(false);
        semestrecoluna.setVisible(false);
        cursocoluna.setVisible(false);
        setorcoluna.setVisible(false);
        aluno.setVisible(false);
        funcionario.setVisible(false);

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {        
        tela.setVisible(false);        
        lista = table.getItems();
         
        Aluno aluno0 = new Aluno();
        aluno0.setNome("Márcia");
        aluno0.setIdade(21);
        aluno0.setEndereco("Rua Domingos Martins");
        aluno0.setFuncao2("Aluno");
        aluno0.setCurso("Medicina");
        aluno0.setSemestre("Terceiro");

        lista.add(aluno0);

        Aluno aluno1 = new Aluno();
        aluno1.setNome("Clara");
        aluno1.setIdade(18);
        aluno1.setEndereco("Rua Navegantes");
        aluno1.setFuncao2("Aluno");
        aluno1.setCurso("Jornalismo");
        aluno1.setSemestre("Sexto");

        lista.add(aluno1);

        Aluno aluno2 = new Aluno();
        aluno2.setNome("Carlos");
        aluno2.setIdade(20);
        aluno2.setEndereco("Rua Maria do Rosário");
        aluno2.setFuncao2("Aluno");
        aluno2.setCurso("Direito");
        aluno2.setSemestre("Primeiro");

        lista.add(aluno2);

        Professor professor0 = new Professor();
        professor0.setNome("Márcio");
        professor0.setIdade(39);
        professor0.setEndereco("Rua Guilherme Schell");
        professor0.setFuncao2("Professor");
        professor0.setDisciplina("Programação");
        professor0.setSalario(7000);

        lista.add(professor0);

        Professor professor1 = new Professor();
        professor1.setNome("Rodrigo");
        professor1.setIdade(35);
        professor1.setEndereco("Rua Major Ernesto Witrock");
        professor1.setFuncao2("Professor");
        professor1.setDisciplina("Engenharia");
        professor1.setSalario(10000);

        lista.add(professor1);

        Professor professor2 = new Professor();
        professor2.setNome("Cimara");
        professor2.setIdade(42);
        professor2.setEndereco("Rua Victor Barretos");
        professor2.setFuncao2("Professor");
        professor2.setDisciplina("Linguas");
        professor2.setSalario(12000);

        lista.add(professor2);

        Funcionario funcionario0 = new Funcionario();
        funcionario0.setNome("Gláucia");
        funcionario0.setIdade(39);
        funcionario0.setEndereco("Rua Guilherme Schell");
        funcionario0.setFuncao2("Diretora");
        funcionario0.setSalario(20000);
        

        lista.add(funcionario0);

        Funcionario funcionario1 = new Funcionario();
        funcionario1.setNome("Glória");
        funcionario1.setIdade(57);
        funcionario1.setEndereco("Rua Victor Barretos Schell");
        funcionario1.setFuncao2("Coordenadora");
        funcionario1.setSalario(25000);
        

        lista.add(funcionario1);

        Funcionario funcionario2 = new Funcionario();
        funcionario2.setNome("Luis");
        funcionario2.setIdade(29);
        funcionario2.setEndereco("Rua Vila Schell");
        funcionario2.setFuncao2("Supervisor");
        funcionario2.setSalario(15000);
        

        lista.add(funcionario2);

    
        nomecoluna.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        idadecoluna.setCellValueFactory(new PropertyValueFactory<>("idade"));      
        enderecocoluna.setCellValueFactory(new PropertyValueFactory<>("endereco"));      
        funcaocoluna.setCellValueFactory(new PropertyValueFactory<>("funcao"));      
        semestrecoluna.setCellValueFactory(new PropertyValueFactory<>("semestre"));      
        cursocoluna.setCellValueFactory(new PropertyValueFactory<>("curso"));      
        setorcoluna.setCellValueFactory(new PropertyValueFactory<>("setor"));      


    }

}
